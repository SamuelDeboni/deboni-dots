#!/bin/sh

cp ~/.zshenv zsh/zshenv
cp ~/.zshrc  zsh/zshrc

cp ~/.config/kak/kakrc kakoune/

cp ~/.config/dwm/autostart.sh dwm/
cp ~/.local/bin/dwm-status dwm/
cp ~/.local/bin/dwm-upd-stats dwm/

cp ~/.config/sxhkd/sxhkdrc sxhkd/

cp ~/.local/bin/volume volume

