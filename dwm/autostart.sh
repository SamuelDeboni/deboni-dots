#!/bin/sh
setxkbmap -model abnt2 -layout br -variant abnt2
xmodmap -e 'clear Lock' #ensures you're not stuck in CAPS on mode
xmodmap -e 'keycode 0x42=Escape' #remaps the keyboard so CAPS LOCK=ESC
 
#volumeicon &
#nm-applet &
xrandr --output VGA1 --auto --left-of LVDS1
nitrogen --restore
#udiskie &
start-pulseaudio-x11 &
dwm-status &
picom &
dunst &
sxhkd &
cmst &
lxqt-policykit-agent &
dropboxd &
xbacklight -set 50
