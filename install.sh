#!/bin/sh


#
# This is a simple install scrip fot my dotfiles
#

# zsh configs

# full is to install everything from a base archlinux install
# but it is not completed
if [[ $1 == 'full' ]] ; then
    sudo pacman -S zsh git curl kakoune xrandr nitrogen picom dunst sxhkd cmst lxqt-policykit-agent
    sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
fi

cp zsh/zshenv ~/.zshenv
cp zsh/zshrc ~/.zshrc

# create directorys
[ -d ~/.config ] || mkdir ~/.config/
[ -d ~/.config/dwm ] || mkdir ~/.config/dwm
[ -d ~/.config/sxhkd ] || mkdir ~/.config/sxhkd
[ -d ~/.config/kak ] || mkdir ~/.config/kak
[ -d ~/.local ] || mkdir ~/.local
[ -d ~/.local/bin ] || mkdir ~/.local/bin

# copy configs

cp kakoune/kakrc ~/.config/kak/kakrc

cp dwm/autostart.sh ~/.config/dwm/autostart.sh
chmod +x ~/.config/dwm/autostart.sh

cp dwm/dwm-status ~/.local/bin/dwm-status
chmod +x ~/.local/bin/dwm-status

cp dwm/dwm-status ~/.local/bin/dwm-upd-status
chmod +x ~/.local/bin/dwm-upd-status

cp volume ~/.local/bin/volume
chmod +x ~/.local/bin/volume

cp sxhkd/sxhkdrc ~/.config/sxhkd/sxhkdrc


